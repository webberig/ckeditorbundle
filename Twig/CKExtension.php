<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 14/02/13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\CKEditorBundle\Twig;

class CKExtension extends \Twig_Extension
{
    /****************************************************************************************************************
     * Utilities
     ****************************************************************************************************************/
    private $env;
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->env = $environment;
    }
    private function getId($name, $params)
    {
        if (isset($params["formId"]))
            $id = $params["formId"] . "_";
        else
            $id = "";
        if (isset($params["group"]))
            $id .= $params["group"] . "_";
        $id .= $name;
        return $id;
    }
    public function getName()
    {
        return 'cl_extension';
    }
    private function output($template, $name, $params = array())
    {
        $id = $this->getId($name, $params);
        if (isset($params["group"])) $name = $params["group"] . "[" . $name . "]";
        return $this->env->render($template, array('name' => $name, 'id' => $id, 'params' => $params));
    }

    public function getFilters() {
        $arr = array();
        foreach ($this->controls as $key => $method)
        {
            $arr[$key] = new \Twig_Filter_Method($this, $method, array('is_safe' => array('html')));
        }
        return $arr;
    }
    public function getFunctions() {
        $arr = array();
        foreach ($this->controls as $key => $method)
        {
            $arr[$key] = new \Twig_Function_Method($this, $method, array('is_safe' => array('html')));
        }
        return $arr;
    }

    /****************************************************************************************************************
     * Definitions
     ****************************************************************************************************************/
    private $controls = array(
        "fWysiwyg" => 'formCKEditor'
    );

    /****************************************************************************************************************
     * Controls
     ****************************************************************************************************************/
    public function formCKEditor($name, $params = array())
    {
        return $this->output('WebberigCKEditorBundle:Form:ckeditor.html.twig', $name, $params);
    }
}