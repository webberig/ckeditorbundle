<?php
namespace Webberig\CKEditorBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

include_once dirname(__FILE__).'/../Resources/public/elfinder/php/elFinderConnector.class.php';
include_once dirname(__FILE__).'/../Resources/public/elfinder/php/elFinder.class.php';
include_once dirname(__FILE__).'/../Resources/public/elfinder/php/elFinderVolumeDriver.class.php';
include_once dirname(__FILE__).'/../Resources/public/elfinder/php/elFinderVolumeLocalFileSystem.class.php';

function access($attr, $path, $data, $volume) {
    return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
        ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
        :  null;                                    // else elFinder decide it itself
}

class elFinderConnector
{
    protected $options = array();
    protected $container = array();

    /**
     * Returns an array of options to configure the elFinder library
     *
     * @return array
     */
    protected function configure()
    {
        $request = $this->container->get('request');
        $base_url = $request->getBaseURL();

        $options = array(
            'roots' => array(
                array(
                    'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
                    'path'          => 'uploads/',         // path to files (REQUIRED)
                    'URL'           => $request->getScheme().'://'.$request->getHttpHost() .  '/uploads/', // URL to files (REQUIRED)
                    'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
                )
            )
        );

        return $options;
    }
    /**
     * The constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->options = $this->configure();

        if(null === $this->options)
        {
            throw new \Exception(sprintf("The configure method cannot return a null value. Check the value returned by the configure method in the %className% object", \get_class($this)));
        }

        if(!is_array($this->options))
        {
            throw new \Exception(sprintf("The configure method must return an array. Check the value returned by the configure method in the %className% object", \get_class($this)));
        }
    }

    /**
     * Starts the elFinder
     */
    public function connect()
    {
        $connector = new \elFinderConnector(new \elFinder($this->options));
        $connector->run();
    }
}