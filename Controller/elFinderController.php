<?php

namespace Webberig\CKEditorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class elFinderController extends Controller
{
    public function connectAction()
    {
        $connector = $this->container->get('webberig.elfinder');
        $connector->connect();

        return "";
    }

    public function showAction()
    {
        if (!is_dir("uploads"))
            mkdir("uploads");
        return $this->render('WebberigCKEditorBundle:elFinder:show.html.twig');

    }
}
